module org.alchemistfrog.pomodoro {
    requires javafx.controls;
    requires javafx.fxml;

    requires com.google.gson;
    requires org.apache.commons.lang3;

    opens org.alchemistfrog.pomodoro to javafx.fxml;
    opens org.alchemistfrog.pomodoro.controller to javafx.fxml;
    opens org.alchemistfrog.pomodoro.model to com.google.gson;

    exports org.alchemistfrog.pomodoro;
    exports org.alchemistfrog.pomodoro.controller;
}