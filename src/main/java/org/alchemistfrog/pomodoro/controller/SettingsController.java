package org.alchemistfrog.pomodoro.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;
import org.alchemistfrog.pomodoro.Launcher;
import org.alchemistfrog.pomodoro.model.Settings;
import org.alchemistfrog.pomodoro.utils.SettingsManager;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class SettingsController implements Initializable {

    @FXML
    private TextField longBreakDuration;

    @FXML
    private TextField rounds;

    @FXML
    private TextField shortBreakDuration;

    @FXML
    private TextField workDuration;

    private static Settings settings = SettingsManager.loadSettings();
    private static boolean saved = true;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        longBreakDuration.setText(String.valueOf(settings.longBreakDuration()));
        rounds.setText(String.valueOf(settings.rounds()));
        shortBreakDuration.setText(String.valueOf(settings.shortBreakDuration()));
        workDuration.setText(String.valueOf(settings.workDuration()));
    }

    @FXML
    void cancel(ActionEvent event) {
        saved = false;
        close(event);
    }

    @FXML
    void save(ActionEvent event) {
        int workDuration = Integer.parseInt(this.workDuration.getText());
        int rounds = Integer.parseInt(this.rounds.getText());
        int shortBreakDuration = Integer.parseInt(this.shortBreakDuration.getText());
        int longBreakDuration = Integer.parseInt(this.longBreakDuration.getText());

        Settings newSettings = new Settings(workDuration, shortBreakDuration, longBreakDuration, rounds);
        SettingsManager.saveSettings(newSettings);

        saved = true;

        close(event);
    }

    public static boolean show(Window owner) {
        settings = SettingsManager.loadSettings();

        Stage stage = new Stage();
        URL fxmlPath = Launcher.class.getResource("fxml/settings-view.fxml");
        FXMLLoader loader = new FXMLLoader(fxmlPath);

        try {
            Scene scene = new Scene(loader.load());
            stage.setScene(scene);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initOwner(owner);
        stage.showAndWait();

        return saved;
    }

    private void close(ActionEvent event) {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.close();
    }

    public static Settings getSettings() {
        return settings;
    }
}
