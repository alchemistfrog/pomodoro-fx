package org.alchemistfrog.pomodoro.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import org.alchemistfrog.pomodoro.model.Settings;
import org.alchemistfrog.pomodoro.utils.Pomodoro;
import org.alchemistfrog.pomodoro.utils.SettingsManager;

import java.net.URL;
import java.util.ResourceBundle;

public class MainController implements Initializable {

    @FXML
    private Label timer;
    @FXML
    private Label currentRound;
    @FXML
    private Button pauseButton;
    @FXML
    private Button startButton;
    @FXML
    private Button stopButton;
    @FXML
    private Label totalRound;
    private Pomodoro pomodoro;
    private Settings settings = SettingsManager.loadSettings();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        pomodoro = new Pomodoro(timer.textProperty(), currentRound.textProperty(), startButton.disableProperty(), pauseButton.disableProperty(), stopButton.disableProperty());

        totalRound.setText(String.valueOf(settings.rounds()));
        pauseButton.setDisable(true);
        stopButton.setDisable(true);
    }

    @FXML
    public void start() {
        pomodoro.start();

        startButton.setDisable(true);
        pauseButton.setDisable(false);
        stopButton.setDisable(false);
    }

    @FXML
    public void pause() {
        pomodoro.pause();

        startButton.setDisable(false);
        pauseButton.setDisable(true);
        stopButton.setDisable(false);
    }

    @FXML
    public void stop() {
        pomodoro.stop();

        startButton.setDisable(false);
        pauseButton.setDisable(true);
        stopButton.setDisable(true);
    }

    @FXML
    public void openSettings(ActionEvent event) {
        boolean saved = SettingsController.show(((Node) event.getSource()).getScene().getWindow());

        if (saved) {
            stop();
            this.settings = SettingsManager.loadSettings();
            currentRound.setText("1");
            totalRound.setText(String.valueOf(settings.rounds()));
        }
    }
}
