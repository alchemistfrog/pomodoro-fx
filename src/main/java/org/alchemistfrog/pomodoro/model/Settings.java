package org.alchemistfrog.pomodoro.model;

public record Settings(int workDuration, int shortBreakDuration, int longBreakDuration, int rounds){}
