package org.alchemistfrog.pomodoro.utils;

import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.StringProperty;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import org.alchemistfrog.pomodoro.model.Settings;
import org.apache.commons.lang3.time.StopWatch;

import java.time.Duration;

public class Pomodoro extends Service<Void> {

    private enum Phases {WORKING_PHASE, SHORT_BREAK_PHASE, LONG_BREAK_PHASE}

    private StringProperty chronoLabelProperty;
    private StringProperty roundCountProperty;
    private BooleanProperty startButtonDisableProperty;
    private BooleanProperty pauseButtonDisableProperty;
    private BooleanProperty stopButtonDisableProperty;
    private Settings settings = SettingsManager.loadSettings();
    private StopWatch stopWatch = new StopWatch();
    private Phases currentPhase = Phases.WORKING_PHASE;
    private int currentRound = 1;

    public Pomodoro(
            StringProperty chronoLabelProperty,
            StringProperty roundCountProperty,
            BooleanProperty startButtonDisableProperty,
            BooleanProperty pauseButtonDisableProperty,
            BooleanProperty stopButtonDisableProperty
    ) {
        this.chronoLabelProperty = chronoLabelProperty;
        this.roundCountProperty = roundCountProperty;
        this.startButtonDisableProperty = startButtonDisableProperty;
        this.pauseButtonDisableProperty = pauseButtonDisableProperty;
        this.stopButtonDisableProperty = stopButtonDisableProperty;
    }

    @Override
    protected Task<Void> createTask() {
        return new Task<>() {
            @Override
            protected Void call() throws Exception {
                while (true) {
                    long phaseDuration = switch (currentPhase) {
                        case WORKING_PHASE -> settings.workDuration() * 60 * 1000;
                        case SHORT_BREAK_PHASE -> settings.shortBreakDuration() * 60 * 1000;
                        case LONG_BREAK_PHASE -> settings.longBreakDuration() * 60 * 1000;
                    };

                    long currentTime = (stopWatch.getTime()/1000) * 1000;
                    boolean isLastRound = currentRound == settings.rounds();
                    boolean phaseIsOver = currentTime >= phaseDuration;

                    if (currentPhase == Phases.WORKING_PHASE && !isLastRound && phaseIsOver) {
                        currentPhase = Phases.SHORT_BREAK_PHASE;
                        reset();
                    } else if (currentPhase == Phases.WORKING_PHASE && isLastRound && phaseIsOver) {
                        currentPhase = Phases.LONG_BREAK_PHASE;
                        reset();
                    } else if (currentPhase == Phases.SHORT_BREAK_PHASE && phaseIsOver) {
                        currentPhase = Phases.WORKING_PHASE;
                        currentRound++;
                        Platform.runLater(() -> roundCountProperty.setValue(String.valueOf(currentRound)));
                        reset();
                    } else if (currentPhase == Phases.LONG_BREAK_PHASE && phaseIsOver) {
                        currentRound = 1;
                        Platform.runLater(() -> {
                            roundCountProperty.setValue(String.valueOf(currentRound));
                            startButtonDisableProperty.setValue(false);
                            pauseButtonDisableProperty.setValue(true);
                            stopButtonDisableProperty.setValue(true);
                        });

                        stop();
                    }

                    Platform.runLater(() -> chronoLabelProperty.setValue(getCurrentTime()));

                    Thread.sleep(1000);
                }
            }
        };
    }

    public void start() {
        if (stopWatch.isSuspended()) {
            stopWatch.resume();
        } else if (stopWatch.isStopped()) {
            stopWatch.reset();
            stopWatch.start();
        } else {
            stopWatch.start();
        }

        if (!isRunning()) {
            super.start();
        }
    }

    public void pause() {
        if (stopWatch.isStarted()) {
            stopWatch.suspend();
        }
    }

    public void stop() {
        if (!stopWatch.isStopped()) {
            stopWatch.stop();
            stopWatch.reset();
        }
    }

    public void reset() {
        stopWatch.stop();
        stopWatch.reset();
        stopWatch.start();
    }

    public String getCurrentTime() {
        Duration duration = Duration.ofMillis(stopWatch.getTime());

        long minutes = duration.toMinutesPart();
        long seconds = duration.toSecondsPart();

        return String.format("%02d:%02d", minutes, seconds);
    }
}
