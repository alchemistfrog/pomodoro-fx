package org.alchemistfrog.pomodoro.utils;

import com.google.gson.Gson;
import org.alchemistfrog.pomodoro.Launcher;
import org.alchemistfrog.pomodoro.model.Settings;

import java.io.*;
import java.util.Objects;

public class SettingsManager {

    public static Settings loadSettings() {
        Gson gson = new Gson();
        Settings settings = new Settings(25, 5, 20, 4);
        File file = new File(Objects.requireNonNull(Launcher.class.getResource("settings/settings.json")).getFile());

        try {
            Reader reader = new FileReader(file);
            settings = gson.fromJson(reader, Settings.class);
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        }

        return settings;
    }

    public static void saveSettings(Settings settings) {
        Gson gson = new Gson();
        File file = new File(Objects.requireNonNull(Launcher.class.getResource("settings/settings.json")).getFile());
        String json = gson.toJson(settings);

        try(FileWriter writer = new FileWriter(file)) {
            writer.write(json);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
